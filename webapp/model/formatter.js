sap.ui.define([], function () {
	"use strict";
	return {
		valueText: function (sVal) {
			var resourceBundle = this.getView().getModel("i18n").getResourceBundle();
			switch (sVal) {
				case "1":
					return resourceBundle.getText("valueBeginner");
				case "2":
					return resourceBundle.getText("valueIntermediate");
				case "3":
					return resourceBundle.getText("valueAdvanced");
				default:
					return sVal;
			}
		},
    dateText: function (sStartM, sStartY, sEndM, sEndY){
      var output = sStartM.substr(0,3) +"' "+ sStartY.substr(2) +" - "+ sEndM.substr(0,3) +"' "+ sEndY.substr(2);
      return output;
    }
	};
});
