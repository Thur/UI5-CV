sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var ProjectBlock = BlockBase.extend("ui5.cv.blocks.ProjectBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ui5.cv.view.ProjectBlock",
					type: "XML"
				},
				Expanded: {
					viewName: "ui5.cv.view.ProjectBlock",
					type: "XML"
				}
			}
		}
	});
	return ProjectBlock;
});
