sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var QualificationBlock = BlockBase.extend("ui5.cv.blocks.QualificationBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ui5.cv.view.Qualification",
					type: "XML"
				},
				Expanded: {
					viewName: "ui5.cv.view.Qualification",
					type: "XML"
				}
			}
		}
	});
	return QualificationBlock;
});
