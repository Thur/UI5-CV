sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var ProfileBlock = BlockBase.extend("ui5.cv.blocks.ProfileBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ui5.cv.view.ProfileBlock",
					type: "XML"
				},
				Expanded: {
					viewName: "ui5.cv.view.ProfileBlock",
					type: "XML"
				}
			}
		}
	});
	return ProfileBlock;
});
