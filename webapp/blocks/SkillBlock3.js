sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var SkillBlock = BlockBase.extend("ui5.cv.blocks.SkillBlock3", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ui5.cv.view.SkillBlock3",
					type: "XML"
				},
				Expanded: {
					viewName: "ui5.cv.view.SkillBlock3",
					type: "XML"
				}
			}
		}
	});
	return SkillBlock;
});
