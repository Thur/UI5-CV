sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var SkillBlock = BlockBase.extend("ui5.cv.blocks.SkillBlock", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ui5.cv.view.SkillBlock",
					type: "XML"
				},
				Expanded: {
					viewName: "ui5.cv.view.SkillBlock",
					type: "XML"
				}
			}
		}
	});
	return SkillBlock;
});
