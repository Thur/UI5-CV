sap.ui.define(['sap/uxap/BlockBase'], function (BlockBase) {
	"use strict";

	var SkillBlock = BlockBase.extend("ui5.cv.blocks.SkillBlock2", {
		metadata: {
			views: {
				Collapsed: {
					viewName: "ui5.cv.view.SkillBlock2",
					type: "XML"
				},
				Expanded: {
					viewName: "ui5.cv.view.SkillBlock2",
					type: "XML"
				}
			}
		}
	});
	return SkillBlock;
});
