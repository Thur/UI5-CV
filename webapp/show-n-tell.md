# Show n Tell notes  
### Intro  
- A while ago i found this: www.rleonardi.com/interactive-resume/
  - An "interactive CV" which is more than just a PDF
- Demonstrates skills rather than describes them
- "I Wish i had the skills to do that"
###### Some Time Passes...
- Looking at the UI5 API reference  
  - because why wouldn't you a Sunday evening  


- found the `SAP.UXAP` library
- Thought it was cool..
- "Hey... i could be an object!"

### A plan emerges
- Want to do more in the UI5 space
- Want to play around with `SAP.UXAP`
- want to keep the JS hacking to a minimum, so i can see the capabilities of the Library
- Want to play with some "bread and butter UI5 facets (Lists, Tables, Layouts)
- Want to further my experience with Git
- Ultimate Goal: UI5 Bluefin CV powered by JSON (based on the Word template we currently use).

### Demo

1) The App  
  a) Walkthrough in desktop mode  
  b) Show off in Mobile mode
2) The Model
3) The View
4) The Javascript

### Lessons Learned
1) Routing can be quite hacky
2) You really need to know the meaning of `>/` and `>` when binding properties
3) Grid Layout FTW!
4) TileContainer requires too much thought to get working

### Version 2
1) use Webpack to productionify
2) Update the content
3) Look at reducing the amount of pure text in the app
4) Edit Mode?
5) Perhaps remove the Names/Logos of delicate clients (or check with Resourcing)

I'm open to any tweaks/improvements.
##### Play Test here:
https://gitlab.com/Thur/UI5-CV  
##### Or Clone the repo. here:  
`git clone https://Thur@gitlab.com/Thur/UI5-CV.git`
