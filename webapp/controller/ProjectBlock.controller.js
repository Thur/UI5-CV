sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "../model/formatter"
], function(Controller, formatter) {
  "use strict";

  return Controller.extend("ui5.cv.controller.ProjectBlock", {
    formatter: formatter,
        onItemPress: function (oEvent) {
      var oItem = oEvent.getSource();
        var oRouter = sap.ui.core.UIComponent.getRouterFor(sap.ui.getCore().byId("__xmlview1").getController());
      var sPath= oItem.getBindingContext("cvModel").getPath();
			oRouter.navTo("detail", {
				projectPath: oItem.getBindingContext("cvModel").getPath().substr(10)
			});
		}
  });
});
