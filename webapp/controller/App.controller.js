sap.ui.define([
  "sap/ui/core/mvc/Controller"
], function(Controller) {
  "use strict";

  return Controller.extend("ui5.cv.controller.App", {
    _oView: null,
    onInit: function(){
      var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
       this._oView = this.getView();

    },
    onTwitterPress: function (btnEvnt) {
      var sTwitter = this._oView.getModel("cvModel").getProperty("/cv/twitter");
        window.open('https://twitter.com/'+sTwitter)
    },
    onLinkedInPress: function (btnEvnt) {
      var sLinkedIn = this._oView.getModel("cvModel").getProperty("/cv/linkedin");
        window.open('https://www.linkedin.com/in/'+sLinkedIn)
    },
    onGitLabPress: function (btnEvnt) {
      var sGit = this._oView.getModel("cvModel").getProperty("/cv/gitlab");
        window.open('https://gitlab.com/'+sGit)
    },
    onBluefinPress: function (btnEvnt) {
        window.open('http://bluefinsolutions.com')
    },
    onEmailPress: function (linkEvnt){
      var sHref = this._oView.getModel("cvModel").getProperty("/cv/email");
      window.location.href =
        "mailto:"
        + encodeURIComponent(sHref)
    },
    onSlackPress: function (btnEvnt){
      var sSlack = this._oView.getModel("cvModel").getProperty("/cv/slack");
      window.open('https://openui5.slack.com/messages/' + encodeURIComponent(sSlack))

    }
  });
});
