sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/m/Dialog",
  "sap/m/Text"
], function(Controller, Dialog, Text) {
  "use strict";

  return Controller.extend("ui5.cv.controller.Qualification", {
    onQualPress: function(oEvent){
      var oTile = oEvent.getSource(),
          oBindingContext = oTile.getBindingContext("cvModel"),
          sPath = oBindingContext.getPath();

    //TODO: Build Dialog Fragment and stick it here with oDialog.bindElement(oBindingContext.getPath())
             var oView = this.getView();
             var oDialog = oView.byId("qualDialog");
             // create dialog lazily
             if (!oDialog) {
                // create dialog via fragment factory
                oDialog = sap.ui.xmlfragment(oView.getId(), "ui5.cv.view.fragments.QualDialog", this);
                oView.addDependent(oDialog);
             }

             oDialog.bindElement({path: sPath , model:"cvModel"});
             oDialog.open();
    },
		onCloseDialog : function () {
			this.getView().byId("qualDialog").close();
		}
  });
});
