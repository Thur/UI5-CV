sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/ui/core/routing/History"
], function(Controller, History) {
  "use strict";

  return Controller.extend("ui5.cv.controller.ProjectDetail", {
    onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(sap.ui.getCore().byId("__xmlview1").getController());
			oRouter.getRoute("detail").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function (oEvent) {
			this.getView().bindElement({
				path: "/projects/" + oEvent.getParameter("arguments").projectPath,
				model: "cvModel"
			});
		},

    onNavBack: function(){
      var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(sap.ui.getCore().byId("__xmlview1").getController());
				oRouter.navTo("app", {}, true);
			}
    }
  });
});
