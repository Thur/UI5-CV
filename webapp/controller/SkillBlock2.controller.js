sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/ui/model/json/JSONModel",
  "../model/formatter"
], function(Controller, JSONModel, formatter) {
  "use strict";

  return Controller.extend("ui5.cv.controller.SkillBlock2", {
    formatter: formatter
  });
});
