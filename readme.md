A CV built in UI5 leveraging the SAP.UXAP library.  
(A sandbox for me to play with different controls)

# WARNING  
This is a very hacky first pass - numerous improvements to be made. Please feel free to add issues w/ improvement suggestions :)

To test:  
- Download the git repo: `git clone https://gitlab.com/Thur/UI5-CV.git`
- Host files on localhost by whichever tool you use (I use XAMPP)
- Click and break stuff :)
    